<?php

function telefonortefunc($gdbcon) {
  echo "<a class='btn btn-primary' target='_blank' href='drucktelefonliste.php'>Telefonliste</a>";
  echo "<table class='table table-hover'>";
  echo "<thead>";
  echo "<th>Kurzwahl</th>";
  echo "<th>Ort</th>";
  echo "<th>Telefon-IP</th>";
  $query = "SELECT * FROM tblbenutzer WHERE fldbelohn='J' ORDER BY fldreihenfolge";
  $result = mysqli_query($gdbcon,$query);
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    echo "<th>".$line['fldbez']."</th>";
  }
  echo "</thead>";
  
  $query = "SELECT * FROM tbltelorte ORDER BY fldsort";
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    echo "<tr>"; 
    echo "<td>".$line['fldsort']."</td>";
    echo "<td>".$line['fldbez']."</td>";
    echo "<td>".$line['fldtelefonip']."</td>";
    $qryben = "SELECT * FROM tblbenutzer WHERE fldbelohn='J' ORDER BY fldreihenfolge";
    $resben = mysqli_query($gdbcon,$qryben);
    while ($linben = mysqli_fetch_array($resben,MYSQLI_ASSOC)) {
      $qryort = "SELECT * FROM tbltelefonorte WHERE fldid_ort=".$line['fldindex']." AND fldid_benutzer=".$linben['fldindex'];
      //echo $qryort."<br>";
      $resort = mysqli_query($gdbcon,$qryort);
      $anwesend="N";
      if ($linort = mysqli_fetch_array($resort,MYSQLI_ASSOC)) {
        $anwesend = $linort['fldanwesend'];	
        //echo $anwesend."=anwesend<br>"; 
      }	
      if ($anwesend=="J") {
        echo "<td><a class='btn btn-".$line['fldfarbe']."' href='telefonorte.php?telefonort=1&ort=".$line['fldindex']."&benutzer=".$linben['fldindex']."'>anwesend</a></td>";
      } else {
        echo "<td><a class='btn btn-default' href='telefonorte.php?telefonort=1&ort=".$line['fldindex']."&benutzer=".$linben['fldindex']."'>abwesend</a></td>";
      }
    }
    echo "</tr>";    
  }
  echo "</table>";  
}

function telefonortwaehlen($gdbcon) {
  $idort=$_GET['ort'];
  $idbenutzer=$_GET['benutzer'];	
  $benutzer="Unbekannt";
  $query="SELECT * FROM tblbenutzer WHERE fldindex=".$idbenutzer;
  $result = mysqli_query($gdbcon,$query);
  if ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    $benutzer=$line['fldbez'];
  }  
  $ort="Unbekannt";
  $query="SELECT * FROM tbltelorte WHERE fldindex=".$idort;
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  if ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    $ort=$line['fldbez'];
  }  
  $query="SELECT * FROM tbltelefonorte WHERE fldid_ort=".$idort." AND fldid_benutzer=".$idbenutzer;
  //echo $query."<br>";	
  $anwesend="N";
  $insert="J";
  $result = mysqli_query($gdbcon,$query);
  if ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
  	 $anwesend=$line['fldanwesend'];
  	 //echo $anwesend."=fldanwesend<br>";
  	 $insert="N";
  }
  if ($anwesend=="N") {
    $anwesend="J";
  } else {
    $anwesend="N";
  }
  if ($anwesend=="J") {
    $qryabw="UPDATE tbltelefonorte SET fldanwesend='N' WHERE fldid_benutzer=".$idbenutzer;
    mysqli_query($gdbcon,$qryabw);
  }
  if ($insert=="J") {
    $statement="INSERT INTO tbltelefonorte (fldanwesend,fldid_ort,fldid_benutzer) VALUES('".$anwesend."',".$idort.",".$idbenutzer.")";
  } else {
  	 $statement="UPDATE tbltelefonorte SET fldanwesend='".$anwesend."' WHERE fldid_ort=".$idort." AND fldid_benutzer=".$idbenutzer;
  }
  mysqli_query($gdbcon,$statement);
  //echo $anwesend."=anwesend<br>";
  if ($anwesend=="J") {
    echo "<div class='alert alert-success'>";
    echo $benutzer." ist im ".$ort." anwesend.<br>"; 
    echo "</div>";
  } else {
    echo "<div class='alert alert-default'>";
    echo $benutzer." ist im ".$ort." abwesend.<br>"; 
    echo "</div>";
  }
  //echo "<a class='btn btn-success' href='telefonorte.php'>Telefonort</a>";
  echo "<meta http-equiv='refresh' content='0; URL=telefonorte.php'>";  
}

?>