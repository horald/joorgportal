<?php
header("content-type: text/html; charset=utf-8");
include("../config.php");
include("bootstrapfunc.php");
bootstraphead();
bootstrapbegin("Welcome");

$versnr=$_GET['versnr'];
$versdat=$_GET['versdat'];

$string = file_get_contents("../version.json");
$json_a = json_decode($string, true);
$localversnr=$json_a['versnr'];
$localversdat=$json_a['versdat'];

$callback=urlencode("http://localhost/own/joorgportal/classes/welcome.php");
$chkupdate="getversion.php?callback=".$callback;
echo "<a href='".$chkupdate."' class='btn btn-primary btn-sm active' role='button'>Update prüfen</a> ";
echo "<a href='help.php?pagename=welcome' class='btn btn-primary btn-sm active' role='button'>Hilfe</a> ";

if ($versnr>$localversnr) {
  echo "<form name='eingabe' class='form-horizontal' method='post' action='downloadupdate.php'>";
  echo "     <button type='submit' name='submit' value='save' class='btn btn-primary'>Update downloaden</button>";
  echo "<input type='hidden' name='versnr' value='".$versnr."'>";
  echo "<input type='hidden' name='versdat' value='".$versdat."'>";
  echo "</form>";
//  echo "<form name='eingabe' class='form-horizontal' method='post' action='finishupdate.php'>";
//  echo "     <button type='submit' name='submit' value='save' class='btn btn-primary'>Update abschliessen</button>";
//  echo "<input type='hidden' name='versnr' value='".$versnr."'>";
//  echo "<input type='hidden' name='versdat' value='".$versdat."'>";
//  echo "</form>";
}

echo "<pre>";
echo "<table>";
if ($versdat<>"") {
  echo "<tr><td>Neuer Stand</td>  <td>: ".$versdat."</td></tr>";
  echo "<tr><td>Neue Version</td><td>: ".$versnr."</td></tr>";
}
echo "<tr><td>lokaler Stand</td>  <td>: ".$localversdat."</td></tr>";
echo "<tr><td>lokale Version</td><td>: ".$localversnr."</td></tr>";
echo "<tr><td>Autoinc-Start/dbsyncnr</td>  <td>: ".$autoinc_start."</td></tr>";
echo "<tr><td>Autoinc-Step</td>  <td>: ".$autoinc_step."</td></tr>";
echo "<tr><td>aktueller User</td>  <td>: ".get_current_user()."</td></tr>";
if (isset($_GET['ipaddr'])) {
  $ipaddr=$_GET['ipaddr'];
} else {
  $ipaddr=$_SERVER['REMOTE_ADDR'];
}
echo "<tr><td>aktueller IP Addr</td>  <td>: ".$ipaddr."</td></tr>";
//echo "<tr><td>aktueller Computername</td>  <td>: ";
//echo "<script>";
//echo "alert(location.hostname);";
//echo "</script>";
//echo "</td></tr>";
echo "<tr><td>aktueller Hostname</td>  <td>: ".gethostname()."</td></tr>";
echo "</table>";
echo "</pre>";

bootstrapend();
?>