<?php
header("content-type: text/html; charset=utf-8");

function hilfefunc($gdbcon,$menu,$idwert,$menuid,$pagename,$pageno,$url) {
if ($pagename=="") {
  if ($pageno=="") {
  	 $pagename=$menu;
    $query="SELECT * FROM tblhelppage WHERE fldpagename='".$menu."'";
  } else {
    $query="SELECT * FROM tblhelppage WHERE fldpageno='".$pageno."'";
  }  
} else {
  $query="SELECT * FROM tblhelppage WHERE fldpagename='".$pagename."'";
}  
mysqli_query($gdbcon,"SET NAMES 'utf8'");
$result = mysqli_query($gdbcon,$query);
$lok=false;
$pageno=0;
if ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
  $lok=true;
  $pageno=$row['fldpageno'];
  $pagename=$row['fldpagename'];
}

if ($typ<>"print") {
  if ($url<>"") {
    echo "<a href='".$url."' class='btn btn-primary btn-sm active' role='button'>zur Anwendung</a> "; 
  } else {
    echo "<a href='showtab.php?menu=".$menu."&idwert=".$idwert."&menuid=".$menuid."' class='btn btn-primary btn-sm active' role='button'>zur Anwendung</a> "; 
  }
  echo "<a href='help.php?menu=".$menu."&pagename=helpindex' class='btn btn-primary btn-sm active' role='button'>Index</a> "; 
  echo "<a href='help.php?menu=".$menu."&pageno=".$pageno."&action=back' class='btn btn-primary btn-sm active' role='button'>zurück</a> "; 
  echo "<a href='help.php?menu=".$menu."&pageno=".$pageno."&action=forward' class='btn btn-primary btn-sm active' role='button'>vor</a> "; 
  echo "<a href='html2pdf.php' target='_blank' class='btn btn-primary btn-sm active' role='button'>Export to PDF</a> "; 
}

echo "<p class='text-justify'>";
if ($lok) { 
  $_SESSION["menu"]=$menu;
  echo "<iframe src='../sites/help/de-DE/".$row['fldhelpurl']."' frameborder='0' height=400 width=800></iframe>";
  echo "<br> Seite ".$pageno;
} else {
  echo "<div class='alert alert-error'>";
  echo "helppage '".$pagename."' nicht gefunden.";	
  echo "</dic>";
}
echo "</p>";

}

?>