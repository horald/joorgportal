<?php

function drucktelefonabfrage($gdbcon) {
  echo "<form class='form-horizontal' method='post' action='drucktelefonliste.php?druck=1'>";
  echo "<fieldset>";

  echo "<div class='control-group'>";
  $query = "SELECT * FROM tbltelorte WHERE fldtelefonip<>'(kein Telefon)'";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input01'>Außer Telefon:</label>";
  echo "            <div class='input'>";
  echo "  <select name='ausser' size='1'>";
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
      echo "<option style='background-color:#c0c0c0;' value=".$line['fldindex'].">".$line['fldbez']."</option>";
  }
  echo "  </select>";
  echo "            </div>";
  echo "          </div>";

  $fusstext="<b>Direktcall mit *47</b><br><br>";
  $fusstext=$fusstext."Beispiel *47 192*168*000*150#<br>";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input01'>Fusstext:</label>";
  echo "            <div class='input'>";
  echo "              <input type='textarea' id='input01' name='fusstext' value='".$fusstext."'>";
  echo "            </div>";
  echo "          </div>";

  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' class='btn btn-primary'>OK</button>";
  echo "     <button class='btn'>Abbruch</button>";
  echo "  </div>";
  echo "</div>";
  echo "</fieldset>";
  echo "</form>";
}

function drucktelefonliste($gdbcon) {
  $ausser=$_POST['ausser'];
  $query = "SELECT * FROM tbltelorte WHERE fldindex=".$ausser;
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  if ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    echo "<b>Für ".$line['fldbez'].":</b><br><br>";	
  }
  echo "<table border='3'>";
  echo "<tr>";
  echo "<th width='250'>Ort</th>";
  echo "<th width='120' style='text-align: center;'>IP</th>";
  echo "<th>Kurzwahl</th>";
  echo "</tr>";
  $query = "SELECT * FROM tbltelorte ORDER BY fldsort";
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  $nr=0;
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
  	 if ($line['fldindex']<>$ausser) {
  	 	if ($line['fldtelefonip']<>'(kein Telefon)') {
  	 	  $nr=$nr+1;
        echo "<tr>";
        echo "<td>".$line['fldbez']."</td>";
        echo "<td style='text-align: center;'>".$line['fldtelefonip']."</td>";
        echo "<td style='text-align: right;'>".$nr."</td>";
        echo "</tr>";
      }
    }
  }  
  echo "</table>";
  echo "<br><br>";
  echo $_POST['fusstext'];
}

?>