<?php
header("content-type: text/html; charset=utf-8");


function showvertical($menu,$id,$verz,$wildcard,$pfad,$host) {
    echo "<table class='table table-hover'>";
    echo "<thead>";
    echo "<th width='5'><input type='checkbox' name='cbuttonAll' value='1'></th>";
    echo "<th>Datei</th>";
    echo "<th>Bild</th>";
    echo "<th>Zeigen</th>";
    echo "<th>Aktion</th>";
    echo "<th>TCopy</th>";
    echo "<th>Del</th>";
    echo "</thead>";  
    $count = 0;
    //echo sizeof($verz)."=anz<br>";
    for ($count = 0; $count <= sizeof($verz) ; $count++) {
    	$entry = $verz[$count];
    //while ( $entry = $verz->read () ) {
      if (($entry!=".") AND ($entry!="..")) {
	     $lWeiter=false;
	     if ($wildcard=="") {
	       $lWeiter=true;
	     }	else {
	       $len=strlen($wildcard) * -1;
	       if (substr($entry,$len)==$wildcard) {
		      $lWeiter=true;
          }
	    }
	    if ($lWeiter==true) {
         //$count = $count + 1; 
         echo "<tr>";
         echo "<input type='hidden' name='idwert".$count."' value=".$entry.">";
         echo "<td width='5'><input type='checkbox' name='cbutton".$count."' value='1'></td>";

         switch ($wildcard) {
           case "jpg":
           case "JPG":
             $htmlpfad = str_replace("/var/www/html","http://".$host,$pfad);
             echo "<td><a href='".$htmlpfad.$entry."'>".htmlentities ($entry)."</a></td>";
             echo "<td><img src='",$htmlpfad.$entry."' width='100' height='100' alt='".$entry."' /></td>";
           break;
           case "mp4":
             $pfad = str_replace("/var/www/html","http://".$host,$pfad);
             echo "<td><a href='".$pfad.$entry."'>".htmlentities ($entry)."</a></td>";
             echo "<td></td>";
           break;
           default;
             echo "<td>".htmlentities ($entry)."</td>";
             echo "<td></td>";
           break;
         }

         echo "<td><a href='showjpg.php?pfad=".urlencode($pfad)."&filename=".urlencode($entry)."' target='_blank' class='btn btn-primary btn-sm active' role='button'>Diashow</a></td>";
         echo "<td><a href='showfile.php?pfad=".urlencode($pfad)."&filename=".urlencode($entry)."' target='_blank' class='btn btn-primary btn-sm active' role='button'>Anzeigen</a></td>";
         echo "<td><a href='tcopy.php?id=".$id."&pfad=".urlencode($pfad)."&filename=".urlencode($entry)."' class='btn btn-primary btn-sm active' role='button'>TCopy</a></td>";
         echo "<td><a href='delfile.php?id=".$id."&pfad=".urlencode($pfad)."&filename=".urlencode($entry)."' class='btn btn-primary btn-sm active' role='button'>Del</a></td>";
         echo "</tr>";
	    }	
     }
   }     
  
   echo "</table>";
}	

function showhorizontal($menu,$id,$verz,$wildcard,$pfad,$host) {
  //echo "<table>";
  //echo "<tr height='100'>";
  echo "<br>";
  for ($count = 0; $count <= sizeof($verz) ; $count++) {
  	 $entry = $verz[$count];
  //while ( $entry = $verz->read () ) {
    if (($entry!=".") AND ($entry!="..")) {
	   $lWeiter=false;
	   if ($wildcard=="") {
	     $lWeiter=true;
	   }	else {
	     $len=strlen($wildcard) * -1;
	     if (substr($entry,$len)==$wildcard) {
	       $lWeiter=true;
        }
	   }
	   if ($lWeiter==true) {
        $htmlpfad = str_replace("/var/www/html","http://".$host,$pfad);
	     //echo "<td width='100' height='100'><img src='",$htmlpfad.$entry."' width='100' height='100' alt='".$entry."' /></td>";
	     echo "<div style='float:left; width 100px'>";
	     echo "<img src='",$htmlpfad.$entry."' width='100' height='100' alt='".$entry."' /><br>";
	     echo "<a href='showjpg.php?filename=".$htmlpfad.$entry."' target='_blank'>".$entry."</a> ";
	     echo "</div>";
      }
    }
  }    
  //echo "</tr>";     
  //echo "</table>";
}

function filemanager($gdbcon,$host) {
  $menu=$_GET['menu'];
  $id=$_GET['idwert'];
  if (isset($_GET['parentid'])) {
  	 $parent="&parentid=".$_GET['parentid'];
  } else {
  	 $parent="";
  }
  $sql="SELECT * FROM tblfilemanager WHERE fldindex=".$id;
  $results = mysqli_query($gdbcon,$sql);
  $row = mysqli_fetch_array($results,MYSQLI_ASSOC);
  $pfad = $row['fldPfad'];
  if ($pfad=="SUBMENU") {
    echo "<meta http-equiv='refresh' content='0; URL=showtab.php?menu=".$menu."&idwert=".$id."&parentid=".$row['fldindex']."'>";
  } else {	
    $wildcard = $row['fldWildcard'];
    //$verz = dir ($pfad);
    $verz = scandir ($pfad);
    $anz=sizeof($verz)-2;
    echo "<div class='alert alert-info'>";
    echo "Importpfad:".$pfad." Wildcard:".$wildcard." Anzahl:".$anz;
    echo "</div>";
    echo "<a href='../index.php' class='btn btn-primary btn-sm active' role='button'>Menü</a> "; 
    echo "<a href='showtab.php?menu=".$menu.$parent."' class='btn btn-primary btn-sm active' role='button'>Zurück</a>"; 
    $qrydar="SELECT * FROM tbldarform WHERE fldindex=".$row['fldid_darform'];
    $resdar = mysqli_query($gdbcon,$qrydar);
    $rowdar = mysqli_fetch_array($resdar,MYSQLI_ASSOC);
    if ($rowdar['fldtyp']=="HORZ") {
      showhorizontal($menu,$id,$verz,$wildcard,$pfad,$host);
    } else {  
      showvertical($menu,$id,$verz,$wildcard,$pfad,$host);
    }
  }
}  

?>