<?php
  include ("../config.php");
  include("bootstrapfunc.php");
  bootstraphead();
  bootstrapbegin("DB-Update");
  $versnrshow=$_GET['versnr'];
  $versnrfile=str_replace(".", "", $versnrshow);
  echo "<a class='btn btn-primary' href='welcome.php'>zurueck</a><br><br>";
  echo "<div class='alert alert-success'>";
  echo "Version : ".$versnrshow."<br>";
  echo "</div>";
  $versquery="SELECT * FROM tblversion WHERE fldkurz>'".$versnrfile."'";
  $versresult = mysqli_query($gdbcon,$versquery);
  if ($versline = mysqli_fetch_array($versresult,MYSQLI_ASSOC)) {
    echo "<div class='alert alert-information'>";
    echo "Diese Version wurde bereits installiert.";  
	echo "</div>";
  } else {
    $filename="../sites/updates/sql/dbupdate_v".$versnrfile.".sql"; 
    if (file_exists($filename)) {
      echo "<div class='alert alert-success'>";
      echo "Die Datei $filename wird eingelesen...<br>";
	  echo "</div>";
      $lines = file( $filename);
      $arrtxt = array();
      $arrtxt[] = "";
  	  $arrind=0;
      foreach ($lines as $line_num => $textline) {
	    $arrtxt[$arrind]=$arrtxt[$arrind].$textline;
	    if (strpos($textline,";")>0) {
	      $arrind=$arrind+1;
          $arrtxt[] = "";
        }	  
	  } 
      for ( $x = 0; $x < count ( $arrtxt )-1; $x++ ) {
        echo "<div class='alert alert-information'>";
	    echo $arrtxt[$x]."<br><br>";
	    echo "</div>";
        mysqli_query($gdbcon,$arrtxt[$x]);
      }	
	  $versnrNeufile=$versnrfile+1;
      date_default_timezone_set("Europe/Berlin");
      $timestamp = time();
      $dbversdat = date("Y-m-d",$timestamp);
      $versnrNeu=$versnrshow+0.001;
      $versdatNeu = date("d.m.Y",$timestamp);
	  $insquery="INSERT INTO tblversion (fldbez,fldkurz,flddatum,fldversion) VALUES ('Version ".$versnrNeu."','".$versnrNeufile."','".$dbversdat."','".$versnrNeu."')";
      mysqli_query($gdbcon,$insquery);
	  $file="../version.json";
      $string = file_get_contents($file);
      $json_a = json_decode($string, true);
	  $json_a['versnr']=$versnrNeu;
	  $json_a['versdat']=$versdatNeu;
	  $current=json_encode($json_a);
      file_put_contents($file, $current);	
      echo "<div class='alert alert-success'>";
      echo "DB-Update durchgeführt...<br>";
	  echo "Neue Version : ".$versnrNeu."<br>";
	  echo "Neues Datum  : ".$versdatNeu."<br>";
      echo "</div>";
    } else {
      echo "<div class='alert alert-warning'>";
      echo "Kein DB-Update für diese Version gefunden!";
	  echo "</div>";
    }
  }
  bootstrapend();  
?>