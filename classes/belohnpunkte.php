<?php
include ("../config.php");
include("belohnpunktefunc.php");
include("bootstrapfunc.php");
bootstraphead();
bootstrapbegin("Belohnungspunkte");
if (isset($_GET['acttag'])) {
  $acttag=$_GET['acttag'];
  $actmon=$_GET['actmon'];
} else {
  $acttag=date("j");
  $actmon=date("n");
  //echo $acttag."=acttag<br>";	
  //$acttag=17;
}
if (isset($_GET['userid'])) {
  $userid=$_GET['userid'];
} else {
  $query="SELECT * FROM tblbenutzer WHERE fldstartuser='J'";	
  $result = mysqli_query($gdbcon,$query);
  if ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    $userid=$line['fldindex'];
  } else {
    $userid=0;  
  }
}  
if (isset($_GET['belohn'])) {
  $belohn = $_GET['belohn'];
  if ($belohn=="uebertrag") {
    belohnuebertrag($userid);
  }
  if ($belohn=="tagzurueck") {
    belohntagzurueck($userid,$acttag,$actmon);
  }
  if ($belohn=="tagvor") {
    belohntagvor($userid,$acttag,$actmon);
  }
  if ($belohn=="monvor") {
    belohnmonvor($userid,$acttag,$actmon);
  }
  if ($belohn=="heute") {
    belohnheute($userid);
  }
} else {
  Belohnung($gdbcon,$userid,$acttag,$actmon);
}
bootstrapend();
?>