<?php

function Belohnung($gdbcon,$userid,$acttag,$actmon) {
  $arrindex = array();
  $arrbez = array();
  $arrdatum = array();
  $arruserpkte = array();
  $arrusergrenze = array();
  $starttag=$acttag-3;
  $endtag=$acttag+3;
  $heutetag=date("d");
  $actjahr=date("Y");
  $tage = array("SO", "MO", "DI", "MI", "DO", "FR", "SA");
  $letzttag = array(31,28,31,30,31,30,31,31,30,31,30,31);

  $query="SELECT * FROM tblbenutzer WHERE fldbelohn='J'";	
  $result = mysqli_query($gdbcon,$query);

  echo "<div id='exTab1' class='container1'>";	
  echo "<ul  class='nav nav-pills'>";
  $count=0;
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
  	 $count=$count+1;
    $arrindex[] = $line['fldindex'];
    $arrbez[] = $line['fldbez'];
    $arruserpkte[] = $line['fldbelohnpkte'];
    $arrusergrenze[] = $line['fldbelohngrenze'];
  	 if ($userid==0) { 
  	   if ($count==1) {
        echo "<li class='active'>";
      } else {
        echo "<li>";
      }
    } else {
  	   if ($userid==$line['fldindex']) {
        echo "<li class='active'>";
      } else {
        echo "<li>";
      }
    }  
    echo "<a  href='#".$line['fldindex']."a' data-toggle='tab'>".$line['fldbez']."</a>";
    echo "</li>";
  }  
  echo "</ul>";

  $nAnz = count($arrindex);
  echo "<div class='tab-content clearfix'>";
  for ( $idx = 0; $idx < $nAnz; $idx++ ) {
  	 if ($userid==0) { 
  	   if ($idx==0) {
        $active=" active";
  	   } else {
  	 	  $active="";
  	   }
  	 } else {
  	 	if ($userid==$arrindex[$idx]) {
        $active=" active";
  	 	} else {
  	 	  $active="";
  	 	}
  	 }  	
    echo "<div class='tab-pane".$active."' id='".$arrindex[$idx]."a'>";

    echo "<a class='btn btn-primary' href='belohnpunkte.php?userid=".$arrindex[$idx]."&belohn=tagzurueck&acttag=".$acttag."&actmon=".$actmon."'>Tag zurück</a> ";
    echo "<a class='btn btn-primary' href='belohnpunkte.php?userid=".$arrindex[$idx]."&belohn=heute'>Heute</a> ";
    echo "<a class='btn btn-primary' href='belohnpunkte.php?userid=".$arrindex[$idx]."&belohn=tagvor&acttag=".$acttag."&actmon=".$actmon."'>Tag vor</a> ";
    echo "<a class='btn btn-primary' href='belohnpunkte.php?userid=".$arrindex[$idx]."&belohn=monvor&acttag=".$acttag."&actmon=".$actmon."'>Woche vor</a> ";
    //echo "<a class='btn btn-primary' href='belohnpunkte.php?userid=".$arrindex[$idx]."&belohn=uebertrag'>Übertrag</a> ";

    echo "<table class='table table-hover'>";
    echo "<thead>";
    echo "<th>Aufgabe</th>";
    echo "<th>BF</th>";
    echo "<th style='text-align:center'>Start</th>";
    $actmonid=$actmon;
    $showtag=$starttag-1;
    for ( $tag = $starttag; $tag <= $endtag; $tag++ ) {
    	$showtag=$showtag+1;
      if ($showtag<1) {
        $actmonid=$actmonid-1;
        if ($actmonid<1) {
          $actmonid=12;
        }
        $showtag=$letzttag[$actmonid-1]+$showtag;
      } 
    	if ($showtag>$letzttag[$actmonid-1]) {
        $actmonid=$actmonid+1;
        if ($actmonid>12) {
          $actmonid=1;
        }
        $showtag=1;
      } 
    	$actmonstr=$actmonid;
    	if ($actmonid<10) {
    	  $actmonstr="0".$actmonstr;	
    	}
    	$showtagstr=$showtag;
    	if ($showtag<10) {
    	  $showtagstr="0".$showtag;	
    	}
      $arrdatum[] = $actjahr.".".$actmonstr.".".$showtagstr;
    	$tagid=date("w", mktime(0, 0, 0, $actmonid, $showtag, $actjahr));
    	if ($tag==$heutetag) {
        echo "<th style='text-align:center;background-color:lightgreen'>".$tage[$tagid]."<br>".$showtagstr.".".$actmonstr.".</th>";
    	} else {
        echo "<th style='text-align:center'>".$tage[$tagid]."<br>".$showtagstr.".".$actmonstr.".</th>";
      }
    }
    echo "<th style='text-align:center'>SUM</th>";
    echo "</thead>";

    $query="SELECT * FROM tblbelohnaufgaben";	
    mysqli_query($gdbcon,"SET NAMES 'utf8'");
    $result = mysqli_query($gdbcon,$query);
    $gessum=0;
    while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
      $qrypkt="SELECT * FROM tblbelohnpunkte WHERE fldid_aufgabe=".$line['fldindex']." AND fldid_user=".$arrindex[$idx];	
      $respkt = mysqli_query($gdbcon,$qrypkt);
      $sum=0;
      $pkt=1;
      $startpkt=0;
      if ($linpkt = mysqli_fetch_array($respkt,MYSQLI_ASSOC)) {
        $pkt=$linpkt['fldpunkte'];
        $startpkt=$linpkt['fldstartpkte'];
        if ($startpkt=="") {
          $startpkt=0;
        }	
      }	
      $belohn = $line['fldbez'];
      $aufgabenid = $line['fldindex'];

      $actmonid=$actmon;
      $showtag=$starttag;
      if ($showtag<1) {
        $actmonid=$actmonid-1;
        if ($actmonid<1) {
          $actmonid=12;
        }      
        $showtag=$letzttag[$actmonid-1]+$showtag;
      }
      $showtagstr=$showtag;
      if ($showtag<10) {
        $showtagstr="0".$showtag;
      }
      $qrysum="SELECT sum(fldpunkte) as Summe FROM tblbelohntermin WHERE fldid_aufgabe=".$line['fldindex']." AND fldid_user=".$arrindex[$idx]." AND flddatum<'".$actjahr."-".$actmonstr."-".$showtagstr."'";
      //echo $qrysum."<br>";	
      $ressum = mysqli_query($gdbcon,$qrysum);
      $startsum=0;
      if ($linsum = mysqli_fetch_array($ressum,MYSQLI_ASSOC)) {
        $startsum=$linsum['Summe'];
      }
      $startpkt=$startpkt+$startsum*$pkt;
      $sum=$startpkt;

      echo "<tr><td>".$line['fldbez']."</td>";
      echo "<td>".$pkt."</td>";
      echo "<td style='text-align:right'>".$startpkt."</td>";
      $datumid=0;
      $showtag=$starttag-1;
      $actmonid=$actmon;
      for ( $tag = $starttag; $tag <= $endtag; $tag++ ) {
        $showtag=$showtag+1;
        if ($showtag<1) {
          $actmonid=$actmonid-1;
          if ($actmonid<1) {
            $actmonid=12;
            $actjahr=$actjahr-1;
          }
          $showtag=$letzttag[$actmonid-1];
        }
        if ($showtag>$letzttag[$actmonid-1]) {
          $actmonid=$actmonid+1;
          if ($actmonid>12) {
          	$actmonid=1;
          	$actjahr=$actjahr+1;
          }
          $showtag=1;
    	  } 
        $actmonstr=$actmonid;
    	  if ($actmonid<10) {
    	    $actmonstr="0".$actmonstr;	
    	  }
    	  if ($showtag<10) {
    	    $showtag="0".$showtag;	
    	  }
    	  $datum=$actjahr."-".$actmonstr."-".$showtag;	
        $qrybelohn="SELECT * FROM tblbelohntermin WHERE fldid_user=".$arrindex[$idx]." AND fldid_aufgabe=".$aufgabenid." AND flddatum='".$datum."'"; 
        $resbelohn = mysqli_query($gdbcon,$qrybelohn);
        $ezlpkt=0;
        $ezlpktstr="_";
        if ($linbelohn = mysqli_fetch_array($resbelohn,MYSQLI_ASSOC)) {
          $ezlpkt=$linbelohn['fldpunkte'];
          if ($ezlpkt==0) {
            $ezlpktstr="_";
          } else {
            $ezlpktstr=$ezlpkt;
          }
        }
        $sum=$sum + $ezlpkt*$pkt;
        if ($tag==$heutetag) {
          echo "<td style='text-align:center;background-color:lightgreen'><a class='btn' href='belohnpopup.php?user=".$arrbez[$idx]."&userid=".$arrindex[$idx]."&belohn=".$belohn."&aufgabenid=".$aufgabenid."&datum=".$arrdatum[$datumid]."&ezlpkt=".$ezlpktstr."&acttag=".$acttag."&actmon=".$actmon."'>".$ezlpktstr."</a></td>";
        } else {
          echo "<td style='text-align:center'><a class='btn' href='belohnpopup.php?user=".$arrbez[$idx]."&userid=".$arrindex[$idx]."&belohn=".$belohn."&aufgabenid=".$aufgabenid."&datum=".$arrdatum[$datumid]."&ezlpkt=".$ezlpktstr."&acttag=".$acttag."&actmon=".$actmon."'>".$ezlpktstr."</a></td>";
        }
        $datumid=$datumid+1;
      }
      echo "<td style='text-align:right;padding-right:5px'>".$sum."</td>";
      echo "</tr>";
      $gessum=$gessum+$sum;
    }
    echo "<tr><td>Summe</td>";
    echo "<td></td>";
    echo "<td></td>";
    for ( $tag = $starttag; $tag <= $endtag; $tag++ ) {
      echo "<td></td>";
    }
    echo "<td style='text-align:right;padding-right:5px'>".$gessum."</td>";
    echo "</tr>";
    echo "</table>";    
    $pktdiff=$gessum-$arrusergrenze[$idx];
    if ($pktdiff>0) {
      echo "<div class='alert alert-success'>";
      echo "Belohnung fällig! ".$pktdiff." Punkte drüber. (Grenze bei ".$arrusergrenze[$idx]." Punkte)";
      echo "</div>";
    } else {
    	$pktdiff=$pktdiff*-1;
      echo "<div class='alert alert-warning'>";
      echo "Keine Belohnung. Es fehlen noch ".$pktdiff." Punkte. (Grenze bei ".$arrusergrenze[$idx]." Punkte)";
      echo "</div>";
    }
   
    echo "</div>";
  }  

  echo "</div>";
  echo "</div>";		

  //echo "<script src'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>";
  //echo "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>";  
  echo "<script src='../includes/js/jquery.min.js'></script>";
  echo "<script src='../includes/js/bootstrap.min.js'></script>";  

}

function belohnuebertrag($userid) {
  echo "Belohnübertrag<br>";
  echo "<a class='btn btn-primary' href='belohnpunkte.php?userid=".$userid."'>zurück</a> ";
}

function belohntagzurueck($userid,$acttag,$actmon) {
  $acttag=$acttag-1;
  echo "<meta http-equiv='refresh' content='0; URL=belohnpunkte.php?userid=".$userid."&acttag=".$acttag."&actmon=".$actmon."'>";
}

function belohntagvor($userid,$acttag,$actmon) {
  $acttag=$acttag+1;
  echo "<meta http-equiv='refresh' content='0; URL=belohnpunkte.php?userid=".$userid."&acttag=".$acttag."&actmon=".$actmon."'>";
}

function belohnmonvor($userid,$acttag,$actmon) {
  $acttag=$acttag+7;
  echo "<meta http-equiv='refresh' content='0; URL=belohnpunkte.php?userid=".$userid."&acttag=".$acttag."&actmon=".$actmon."'>";
}

function belohnheute($userid) {
  echo "<meta http-equiv='refresh' content='0; URL=belohnpunkte.php?userid=".$userid."'>";
}

?>