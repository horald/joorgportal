﻿<?php
include("bootstrapfunc.php");
include("lernenfunc.php");
include("../config.php");
$menu=$_GET['menu'];
$idwert=$_GET['idwert'];
include("../sites/views/wp_".$menu."/showtab.inc.php");
bootstraphead();
bootstrapbegin("Lernen");
echo "<a href='showtab.php?menu=".$menu."&idwert=".$idwert."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a> ";
$lernen = $_GET['lernen'];
$reset = $_POST['zuruecksetzen'];
//echo $reset."=reset<br>";
if ($reset=="on") {
  $lektion=$_POST['lektion'];
  $fremdsprache=$pararray['fremdsprache'];
  zuruecksetzen($gdbcon,$lektion,$fremdsprache);
} else {  	
  $muttersprache=$pararray['muttersprache'];
  $fremdsprache=$pararray['fremdsprache'];
  if ($lernen==1) {
    $sprache=$_POST['sprache'];
    $lektion=$_POST['lektion'];
    lernen($gdbcon,$menu,$idwert,$sprache,$lektion,$muttersprache,$fremdsprache);
  } else {
    if ($lernen==2) {
      $vokabel=$_POST['vokabel'];
	   $fldindex=$_POST['fldindex'];
	   $sprache=$_POST['sprache'];
	   $lektion=$_POST['lektion'];
      ergebnis($gdbcon,$menu,$idwert,$vokabel,$fldindex,$sprache,$lektion,$muttersprache,$fremdsprache);
    } else {
      auswahl($gdbcon,$menu,$idwert,$muttersprache,$fremdsprache);
    }
  }
}
bootstrapend();
?>
