<?php

function famkal($gdbcon,$actmon) {
  $tage = array("SO", "MO", "DI", "MI", "DO", "FR", "SA");
  $letzterTag = array(31,28,31,30,31,30,31,31,30,31,30,31);
  $heutetag=date("d");
  $heutemon=date("n");
  $jahr=2019;

  $qryuser="SELECT * FROM tblbenutzer WHERE fldfamkal='J' ORDER BY fldreihenfolge";
  $resuser = mysqli_query($gdbcon,$qryuser);

  echo "<a class='btn btn-primary' href='famkal.php?userid=".$arrindex[$idx]."&famcal=monatzurueck&actmon=".$actmon."'>Monat zurück</a> ";
  echo "<a class='btn btn-primary' href='famkal.php?userid=".$arrindex[$idx]."&famcal=monatvor&actmon=".$actmon."'>Monat vor</a> ";

  echo "<table class='table table-hover' >";
  echo "<thead>";
  echo "<th style='width:25px'></th>";
  echo "<th>".$jahr."</th>";
  while ($linuser = mysqli_fetch_array($resuser,MYSQLI_ASSOC)) {
    echo "<th>".$linuser['fldbez']."</th>";
  }
  echo "</thead>";

  //$endtag=30;
  $intmon=intval($actmon);
  //echo $letzterTag[$intmon]."=letzterTag<br>";
  $endtag=$letzterTag[$intmon-1];
  for ( $tag = 1; $tag <= $endtag; $tag++ ) {
    $tagid=date("w", mktime(0, 0, 0, $intmon, $tag, $jahr));
  	 $tagstr=$tag;
  	 if ($tag<10) {
      $tagstr="0".$tagstr;
  	 }
    $motag=$actmon."-".$tagstr;
    $datum=$jahr."-".$motag;
    $qryuser="SELECT * FROM tblbenutzer WHERE fldfamkal='J' ORDER BY fldreihenfolge";
    $resuser = mysqli_query($gdbcon,$qryuser);
    if ($tag==$heutetag and $intmon==$heutemon) {
      echo "<tr style='background-color:yellow'>";
    } else {
      if ($tage[$tagid]=="SO") {
        echo "<tr style='background-color:lightgray'>";
      } else {
        echo "<tr>";
      }  
    }      
    echo "<td>".$tage[$tagid]."</td>";
    echo "<td>".$tagstr.".".$actmon.".</td>";
    while ($linuser = mysqli_fetch_array($resuser,MYSQLI_ASSOC)) {
    	$userid=$linuser['fldindex'];
    	$terminbez="__________";
    	$bgcolor="white";
      if ($linuser['fldgebkal']=="J") {
        $qrygeb="SELECT * FROM tblgeburtstage WHERE fldGebDatum like '____-".$motag."'";
        $resgeb = mysqli_query($gdbcon,$qrygeb);
        if ($lingeb = mysqli_fetch_array($resgeb,MYSQLI_ASSOC)) {
          $terminbez=$lingeb['fldVorname'];
          $bgcolor="lightgreen";
        }
      }    	
      $qrytermin="SELECT * FROM tbltermine_liste WHERE fldid_user=".$userid." AND fldvondatum='".$datum."'";
      //echo $qrytermin."<br>";
      $restermin = mysqli_query($gdbcon,$qrytermin);
      if ($lintermin = mysqli_fetch_array($restermin,MYSQLI_ASSOC)) {
        $terminbez=$lintermin['fldbez'];
        if ($terminbez=="") { 
          $terminbez="__________";
        }  
        $bgcolor="lightblue";	
      }
      if ($terminbez<>"__________") {
        echo "<td style='background-color:".$bgcolor."'><a class='btn' href='famkaleintrag.php?userid=".$userid."&datum=".$datum."&bez=".$terminbez."&actmon=".$actmon."'>".$terminbez."</a></td>";
      } else {
        echo "<td><a class='btn' href='famkaleintrag.php?userid=".$userid."&datum=".$datum."&bez=".$terminbez."&actmon=".$actmon."'>".$terminbez."</a></td>";
      }
    }
    echo "</tr>";
  }

  echo "</table>";
  
}

?>