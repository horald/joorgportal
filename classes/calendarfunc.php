<?php
header("content-type: text/html; charset=utf-8");

function showcalendar($gdbcon,$heutetag,$heutemon,$heutejahr,$aktmon,$aktjahr) {
  $lastmon = array(31,28,31,30,31,30,31,31,30,31,30,31);
  $monname = array('Januar','Februar','März','April','Mai','Juni','Juli','August','September','Oktober','November','Dezember');
  $wochentage = array('Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag');
  $aktmonname=$monname[$aktmon-1];
  $erster=$aktjahr."-".$aktmon."-01";
  $zeit = strtotime($erster);
  $wotag=$wochentage[date("w", $zeit)];
  $wotagnr=date("w", $zeit);
  $akttag=1;
  if ($wotagnr>1) {
    $aktmon=$aktmon-1;
    if ($aktmon<1) {
      $aktmon=12;
    }
    $akttag=$lastmon[$aktmon-1]-$wotagnr+2;
  }
  if ($wotagnr==0) {
    $aktmon=$aktmon-1;
    if ($aktmon<1) {
      $aktmon=12;
    }
    $akttag=$lastmon[$aktmon-1]-5;
  }

  echo "<table class='table table-bordered'>";
  //echo "<tr><td class='info'>".$aktmonname." ".$aktjahr." ".$erster." ".$wotag." ".$wotagnr."</td></tr>";
  echo "<tr><td class='info'>".$aktmonname." ".$aktjahr."</td></tr>";
  echo "</table>";
  //var_dump($lastmon);
  echo "<table class='table table-bordered'>";
  echo "<tr>";
  echo "<td class1='success col-3' style='width:10%'>Mo</td>";
  echo "<td class1='success col-3' style='width:10%'>Di</td>";
  echo "<td class1='success col-3' style='width:10%'>Mi</td>";
  echo "<td class1='success col-3' style='width:10%'>Do</td>";
  echo "<td class1='success col-3' style='width:10%'>Fr</td>";
  echo "<td class1='success col-3' style='width:10%'>Sa</td>";
  echo "<td class1='success col-3' style='width:10%'>So</td>";
  echo "</tr>";
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  for ($woch = 1; $woch <= 6; $woch++) {  
    echo "<tr>";
    for ($tag = 0; $tag<7; $tag++) {
    	if ($akttag>$lastmon[$aktmon-1]) {
        $akttag=1;
        $aktmon=$aktmon+1;
        if ($aktmon>12) {
        	 $aktmon=1;
        }
    	}
      if ($aktjahr==$heutejahr AND $aktmon==$heutemon AND $akttag==$heutetag) {
        echo "<td class1='danger col-3' style='background-color:orange'>";      
      } else {
        echo "<td class1='col-3'>";      
      }
      echo $akttag."<br>";


      $sql = "SELECT * FROM tbltermin_lst";
      $results = mysqli_query($gdbcon,$sql);
      while ($row = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
        $termin=$row['fldvondatum'];
        $bez=$row['fldbez'];	
        $termintag=substr($termin,-2);
        $terminmon=substr($termin,5,2);
        $terminjahr=substr($termin,0,4);
        //echo "#".$terminmon."#";

        $ljaehrlich=false;
        $backgroundcolor='#0000ff';
        $textcolor="#ffffff";
        $sqlgrp = "SELECT * FROM tbltermine_grp WHERE fldindex=".$row['fldid_terminegrp'];
        $resgrp = mysqli_query($gdbcon,$sqlgrp);
        if ($rowgrp = mysqli_fetch_array($resgrp,MYSQLI_ASSOC)) {
          if ($rowgrp['fldfarbe']<>"") {
            $backgroundcolor=$rowgrp['fldfarbe']; 
          }
          if ($rowgrp['fldtxtfarbe']<>"") {
            $textcolor=$rowgrp['fldtxtfarbe']; 
          }
          if ($rowgrp['fldjaehrlich']=="J") {
            $ljaehrlich=true; 
          }
        }
        $lTerminOK=false;
        if ($ljaehrlich) {
          if ($akttag==$termintag AND $aktmon==$terminmon) {
        	   $lTerminOK=true;
          }
        } else {
          if ($akttag==$termintag AND $aktmon==$terminmon AND $aktjahr==$terminjahr) {
        	   $lTerminOK=true;
          }
        }  	
        if ($lTerminOK) {
          $sqlser = "SELECT * FROM tbltermine_serie WHERE fldindex=".$row['fldid_terminserie'];
//echo $sqlser."<br>";
          $resser = mysqli_query($gdbcon,$sqlser);
          $lweiter=true;
          if ($rowser = mysqli_fetch_array($resser,MYSQLI_ASSOC)) {
          	if ($rowser['fldsel']=="N") {
          	  $lweiter=false;	
          	}
            if ($rowser['fldfarbe']<>"") {
              $backgroundcolor=$rowser['fldfarbe']; 
            }
            if ($rowser['fldtxtfarbe']<>"") {
              $textcolor=$rowser['fldtxtfarbe']; 
            }
          }
          if ($lweiter) {
            //echo "<a href='update.php?menu=termine&idwert=".$row['fldindex']."&callback=calendar'  class='btn btn-primary btn-sm active' role='button'>".$bez."</a>";
            //echo "<div style='background-color:".$backgroundcolor.";'>";
            echo "<a href='update.php?menu=termine&idwert=".$row['fldindex']."&callback=calendar' ><button style='background-color:".$backgroundcolor.";color:".$textcolor.";border-radius: 5px;'>".$bez."</button></a><br>";
            //echo "</div>";
          }  
        }
      }
      echo "</td>";
    	$akttag=$akttag+1;
    }  
    echo "</tr>";
  }  
  echo "</table>";
}  
?>