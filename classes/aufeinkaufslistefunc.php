<?php
session_start();

function aufeinkaufsliste($gdbcon,$menu,$idwert) {
  $dbselarr = $_SESSION['DBSELARR'];
  $count=sizeof($dbselarr);
  echo "<form class='form-horizontal' method='post' action='aufeinkaufsliste.php?menu=".$menu."&idwert=".$idwert."&aufeinkaufsliste=1'>";

  $query = "SELECT * FROM tblorte WHERE fldo01typ='FREMD'";
  echo "          <div class='control-group'>";
  echo "            <label class='control-label' style='text-align:left' for='input01'>Kaufort:</label>";
  echo "            <div class='input'>";
  echo "  <select name='kaufort' size='1'>";
  $result = mysqli_query($gdbcon,$query);
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    $bez = $line['fldBez'];
    echo "<option style='background-color:#c0c0c0;' >".$bez."</option>";
  }
  echo "  </select>";
  echo "            </div>";
  echo "          </div>";

  echo "<table class='table table-hover'>";
  echo "<thead>";
  echo "<th> X</th>";
  echo "<th>Menge</th>";
  echo "<th>Bezeichnung</th>";
  echo "</thead>";
  for ( $x = 0; $x < $count; $x++ ) {
    $query = "SELECT * FROM tblrezeptzutaten WHERE fldindex=".$dbselarr[$x];
    mysqli_query($gdbcon,"SET NAMES 'utf8'");
    $result = mysqli_query($gdbcon,$query);
    $line = mysqli_fetch_array($result,MYSQLI_ASSOC);
    echo "<tr>"; 
    echo "<input type='hidden' name='idwert".$x."' value=".$line['fldindex'].">";
    echo "<td><input type='checkbox' name='check".$x."' value='1'></td>";
    echo "<td>".$line['fldmenge']." ".$line['fldmengeinheit']."</td>";
    echo "<td>".$line['fldbez']."</td>";
    echo "</tr>";
  }
  echo "</table>";
  echo "<input type='hidden' name='count' value=".$count."/>";
  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' class='btn btn-primary'>OK</button>";
  echo "     <button class='btn'>Abbruch</button>";
  echo "  </div>";
  echo "</form>";
}

function speicher_aufeinkaufsliste($gdbcon,$menu,$idwert) {
  echo "<a class='btn btn-primary' href='showtab.php?menu=".$menu."&idwert=".$idwert."'>zurück</a><br>";
  $count = $_POST['count'];
  $cnt=0;
  if ($count>0) {
    $kaufort=$_POST['kaufort'];
    //echo $kaufort."=kaufort<br>";
    for($zaehl = 0; $zaehl < $count; $zaehl++) {
      $idcheck = $_POST['check'.$zaehl];
      if ($idcheck==1) {
	$cnt=$cnt+1;
        $idwert = $_POST['idwert'.$zaehl];
        //echo $idwert."=idwert<br>";  
        $query = "SELECT * FROM tblrezeptzutaten WHERE fldindex=".$idwert;
        mysqli_query($gdbcon,"SET NAMES 'utf8'");
        $result = mysqli_query($gdbcon,$query); 
        $line = mysqli_fetch_array($result,MYSQLI_ASSOC); 
        $qryins="INSERT INTO tblEinkauf_liste (fldBez,fldOrt,fldAnz,fldKonto) VALUES ('".$line['fldbez']."','".$kaufort."','".$line['fldmenge']."','LEBEN')";
        //echo $qryins."<br>";
        $resins = mysqli_query($gdbcon,$qryins);    
        echo "<div class='alert alert-success'>";
        echo $cnt.".) ".$line['fldmenge']." ".$line['fldmengeinheit']." ".$line['fldbez']." bei ".$kaufort." eingetragen.";
        echo "</div>";
      }
    } 
  }
  
}

?>