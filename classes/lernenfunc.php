﻿<?php
header("content-type: text/html; charset=utf-8");

function ergebnis($gdbcon,$menu,$idwert,$vokabel,$fldindex,$sprache,$lektion,$muttersprache,$fremdsprache) {
  $sql="SELECT * FROM tblvokabeln WHERE fldindex=".$fldindex;
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $results = mysqli_query($gdbcon,$sql);
  $row = mysqli_fetch_array($results,MYSQLI_ASSOC);
  $anzgelernt=$row['fldanzgelernt'];
  $anzfehler=$row['fldanzfehler'];
  echo "<br>";
  echo "<table class='table'>";
  if ($sprache==="deuhrv") {
    $ergebnis=$row['fldsprache_zwei'];
    echo "<tr><td>".$muttersprache."</td><td>".$row['fldsprache_eins']."</td></tr>";
    echo "<tr><td><b>".$fremdsprache."</b></td><td><b>".$row['fldsprache_zwei']."</b></td></tr>";
  } else {
    $ergebnis=$row['fldsprache_eins'];
    echo "<tr><td>".$fremdsprache."</td><td>".$row['fldsprache_zwei']."</td></tr>";
    echo "<tr><td><b>".$muttersprache."</b></td><td><b>".$row['fldsprache_eins']."</b></td></tr>";
  }  
  echo "</table>";
  if ($vokabel===$ergebnis) {
  	 $anzgelernt=$anzgelernt+1;
    $update="UPDATE tblvokabeln SET fldanzgelernt=".$anzgelernt." WHERE fldindex=".$fldindex;
    echo "<div class='alert alert-success'>";
    echo "Sehr gut! (".$anzgelernt." x richtig, ".$anzfehler." x falsch)<br>";
    echo "</div>";
  } else {
  	 $anzfehler=$anzfehler+1;
    $update="UPDATE tblvokabeln SET fldanzfehler=".$anzfehler." WHERE fldindex=".$fldindex;
    echo "<div class='alert alert-danger'>";
    echo "Leider falsch! (".$anzgelernt." x richtig, ".$anzfehler." x falsch)<br>";
    echo "</div>";
  }
  //echo $update."<br>";
  mysqli_query($gdbcon,$update);
  echo "<form class='form-horizontal' method='post' action='lernen.php?lernen=1&menu=".$menu."&idwert=".$idwert."'>";
  echo "<input type='hidden' name='sprache' value='".$sprache."' />";
  echo "<input type='hidden' name='lektion' value='".$lektion."' />";
  echo "<input type='submit' value='weiter' />";
  echo "</form>";
  //echo $sprache."=sprache, ".$lektion."<br>";
}

function lernen($gdbcon,$menu,$idwert,$sprache,$lektion,$muttersprache,$fremdsprache) {
  //zufallsdatensatz ermitteln
  if ($lektion==="(ohne)") {
    $where="";
  } else {
    $where=" AND fldlektion='".$lektion."'";
    $qryflt="SELECT * FROM tblfilter WHERE fldtablename='tbllektion' AND fldmaske='LERNEN'";
    //echo $qryflt."<br>";
    $resflt = mysqli_query($gdbcon,$qryflt);
    if ($rowflt = mysqli_fetch_array($resflt,MYSQLI_ASSOC)) {
      $qryflt="UPDATE tblfilter SET fldwert='".$lektion."' WHERE fldtablename='tbllektion' AND fldmaske='LERNEN'";
    } else {
      $qryflt="INSERT INTO tblfilter (fldtablename,fldmaske,fldwert) VALUES('tbllektion','LERNEN','".$lektion."')";
    }
    //echo $qryflt."<br>";
    mysqli_query($gdbcon,$qryflt);
  }

  $sql="SELECT count(*) as anz FROM tblvokabeln WHERE fldfremdsprache='".$fremdsprache."' AND fldanzgelernt-fldanzfehler<=3".$where;
  //echo $sql."<br>";
  $results = mysqli_query($gdbcon,$sql);
  if ($row = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
    $anz=$row['anz'];
    echo "<div class='alert alert-success'>";
    echo "Noch ".$anz." Vokablen. (".$lektion.") für ".$fremdsprache."<br>";
    echo "</div>";
  } else {
    echo "<div class='alert alert-success'>";
    echo "Keine Vokabeln für ".$fremdsprache." gefunden.<br>";
    echo "</div>";
  }
  
  //echo $anz."=anz<br>";
  $sql="SELECT * FROM tblvokabeln WHERE fldfremdsprache='".$fremdsprache."' AND fldanzgelernt-fldanzfehler<=3".$where;
  $results = mysqli_query($gdbcon,$sql);
  $cnt=0;
  //echo mt_rand(1, 2)."=zufall<br>";
  $zufall=mt_rand(1, $anz);
  echo "<br>";
  while ($row = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
    $cnt=$cnt+1;
    //echo $cnt.",".$zufall.",".$row['fldindex']."=cnt,zufall,fldindex<br>";
	 if ($cnt===$zufall) {
	   $fldindex=$row['fldindex'];
	 }
  }
  
  $sql="SELECT * FROM tblvokabeln WHERE fldindex=".$fldindex;
  //echo $sql."<br>";
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $results = mysqli_query($gdbcon,$sql);
  echo "<form class='form-horizontal' method='post' action='lernen.php?lernen=2&menu=".$menu."&idwert=".$idwert."'>";
  echo "<table class='table'>";
  while ($row = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
    if ($sprache==="deuhrv") {
      echo "<tr><td>".$muttersprache."</td><td>".$row['fldsprache_eins']."</td></tr>";
	  echo "<tr><td>".$fremdsprache."</td><td><input type='text' name='vokabel'></td></tr>";
	} else {
      echo "<tr><td>".$muttersprache."</td><td><input type='text' name='vokabel'></td></tr>";
	  echo "<tr><td>".$fremdsprache."</td><td>".$row['fldsprache_zwei']."</td></tr>";
	}
	echo "<input type='hidden' name='fldindex' value=".$row['fldindex']." />";
  }
  echo "</table>";
  echo "<input type='hidden' name='sprache' value='".$sprache."' />";
  echo "<input type='hidden' name='lektion' value='".$lektion."' />";
  echo "<input type='submit' value='Ergebnis' />";
  echo "</form>";
  //echo $sprache."=sprache, ".$lektion."<br>";
}

function zuruecksetzen($gdbcon,$lektion,$fremdsprache) {
  $anzds=0;
  $sqlsel="SELECT count(*) as anz FROM tblvokabeln WHERE fldfremdsprache='".$fremdsprache."' and fldlektion='".$lektion."'";
  $resflt = mysqli_query($gdbcon,$sqlsel);
  if ($rowflt = mysqli_fetch_array($resflt,MYSQLI_ASSOC)) {
    $anzds=$rowflt['anz'];
  }  
  $sqlupd="UPDATE tblvokabeln SET fldanzgelernt=0, fldanzfehler=0 WHERE fldfremdsprache='".$fremdsprache."' and fldlektion='".$lektion."'";
//  echo $sqlupd."<br>";
  mysqli_query($gdbcon,$sqlupd);
  echo "<div class='alert alert-info'>";
  switch ($anzds) {
    case 0:
      echo "Bei ".$lektion." wurde kein Datensatz zurückgesetzt.";
      break;
    case 1:
      echo $lektion." wurde mit 1 Datensatz zurückgesetzt.";
      break;
    default:  
      echo $lektion." wurde mit ".$anzds." Datensätzen zurückgesetzt.";
  }  
//  echo $lektion." wurde zurückgesetzt mit 1 Datensätzen.";
  echo "</div>";  
}

function auswahl($gdbcon,$menu,$idwert,$muttersprache,$fremdsprache) {
  $qryflt="SELECT * FROM tblfilter WHERE fldtablename='tbllektion' AND fldmaske='LERNEN'";
  $resflt = mysqli_query($gdbcon,$qryflt);
  if ($rowflt = mysqli_fetch_array($resflt,MYSQLI_ASSOC)) {
    $auswahl=$rowflt['fldwert'];
  }
  $sql="SELECT * FROM tbllektion";
  $results = mysqli_query($gdbcon,$sql);
  echo "<form class='form-horizontal' method='post' action='lernen.php?lernen=1&menu=".$menu."&idwert=".$idwert."'>";
  echo "Lektion: ";
  echo "<select name='lektion' size='1'>"; 
  echo "<option>(ohne)</option>"; 
  while ($row = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
  	 if ($row['fldbez']===$auswahl) {
      echo "<option selected>".$row['fldbez']."</option>"; 
    } else {
      echo "<option>".$row['fldbez']."</option>"; 
    }
  }
  echo "</select>";
  echo "<br>";  
  echo "  <input type='radio' id='deuhrv' name='sprache' value='deuhrv' checked>";
//  echo "  <label for='deuhrv'> ".$muttersprache." - ".$fremdsprache."</label><br>";
  echo " ".$muttersprache." - ".$fremdsprache."<br>";
  echo "  <input type='radio' id='hrvdeu' name='sprache' value='hrvdeu'>";
//  echo "  <label for='hrvdeu'> ".$fremdsprache." - ".$muttersprache."</label><br>";
  echo " ".$fremdsprache." - ".$muttersprache."<br>";
  echo "  <input type='checkbox' name='zuruecksetzen'> Ergebnis zurücksetzen<br><br>";
  echo "  <input type='submit' value='auswählen' />";
  echo "</form>";
}
  
?>