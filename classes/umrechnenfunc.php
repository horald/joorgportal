<?php
header("content-type: text/html; charset=utf-8");

function abfrage_umrechnen($gdbcon,$pararray,$menu,$idwert) {
  $query = "SELECT * FROM ".$pararray['dbtable']." WHERE ".$pararray['fldindex']."='$idwert'";
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  $line = mysqli_fetch_array($result,MYSQLI_ASSOC);
  echo "<form name='eingabe' class='form-horizontal' method='post' action='umrechnen.php?umrechnen=1&menu=".$menu."&idwert=".$idwert."' enctype='multipart/form-data'>";
  echo "Soll jetzt das ".$line['fldBez']."-Rezept wirklich auf ".$line['fldportionen']." Portion(en) umgerechnet werden?";
  echo "          <div class='control-group'>";
  echo "            <div class='checkbox'>";
  echo "              <input type='checkbox' name='errmsg'> Zeig Fehlermeldung";
  echo "            </div>";
  echo "          </div>";
  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' value='save' class='btn btn-primary'>Speichern</button>";
  echo "     <button class='btn'>Abbruch</button>";
  echo "  </div>";
  echo "<input type='hidden' name='portion' value='".$line['fldportionen']."'>";
  echo "<input type='hidden' name='bez' value='".$line['fldBez']."'>";
  echo "</form>";
}

function speicher_umrechnung($gdbcon,$pararray,$menu,$idwert,$portion,$bez) {
  echo "<a class='btn btn-primary' href='showtab.php?menu=".$menu."&idwert=".$idwert."'>zurueck</a><br>";
  echo "<div class='alert alert-success'>";
  echo $bez." von 4 in ".$portion." Portion(en)";
  echo "</div>";
  $query = "SELECT * FROM tblrezeptzutaten WHERE fldid_rezept=$idwert";
  //echo $query;
  mysqli_query($gdbcon,"SET NAMES 'utf8'");
  $result = mysqli_query($gdbcon,$query);
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    if ($line['fldorgmeng']==0) {
      $menge=$line['fldmenge'];
    } else {
      $menge=$line['fldorgmeng'];
    }
    $orgmeng=$menge;
    $pos = strpos($menge, "/");
    if ($pos>0) {
      $links=substr($menge,0,$pos);
      $rechts=substr($menge,$pos+1);
      $menge=$links / $rechts;
      //echo $pos." l=".$links." r=".$rechts." m=".$menge."<br>";
    }
    $neue_menge=$menge / 4 * $portion;
    if ($neue_menge==0.25) {
      $neue_menge="1/4";
    }
    if ($neue_menge==0.5) {
      $neue_menge="1/2";
    }
    if ($neue_menge==0.75) {
      $neue_menge="3/4";
    }
    if ($neue_menge==0) {
      $neue_menge="";
    }
    echo "<div class='alert alert-info'>";
    echo $menge." ".$line['fldmengeinheit']." ".$line['fldbez']." in ".$neue_menge." ".$line['fldmengeinheit'];
    echo "</div>";
    $qryupdate="UPDATE tblrezeptzutaten SET fldmenge='".$neue_menge."', fldorgmeng='".$orgmeng."' WHERE fldindex=".$line['fldindex'];
    //echo $qryupdate;
    mysqli_query($gdbcon,$qryupdate);
  }
}

?>