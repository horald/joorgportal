<?php
include("bootstrapfunc.php");
include("dbtool.php");
bootstraphead('');
bootstrapbegin("Datenaustausch",'');
$callbackurl=$_POST['callbackurl'];
$dbtable=$_POST['dbtable'];
$dbtyp=$_POST['dbtyp'];
$database=$_POST['database'];
$dbuser=$_POST['dbuser'];
$dbpassword=$_POST['dbpassword'];
$utf8encode=$_POST['utf8encode'];
$dbcolumn=json_decode($_POST['dbcolumn']);
$debug=$_GET['debug'];
if ($debug="J") {
  echo "<div class='alert alert-info'>";
  echo "dbtyp=".$dbtyp."<br>";
  echo "utf8encode=".$utf8encode."<br>";
  echo "database=".$database."<br>";
  echo "dbuser=".$dbuser."<br>";
  echo "dbpassword=".$dbpassword."<br>";
  echo "</div>";
}
echo "<a href='".$callbackurl."'  class='btn btn-primary btn-sm active' role='button'>Zurück</a> ";

echo "<form class='form-horizontal' method='post' action='".$callbackurl."'>";
echo "<input type='hidden' name='status' value='einspielen' />";
echo "<br>";

//$query="SELECT * FROM tblsyncstatus WHERE fldtable='".$dbtable."' AND flddbsyncnr=".$arrdbnchsyncnr[$tablecount];

$dbnchopen=dbopentyp($dbtyp,$database,$dbuser,$dbpassword);
$column=getdbcolumn($dbtyp,$database,$dbtable,$dbuser,$dbpassword)."x";
echo $column."<br>";   
echo $dbcolumn."<br>";
if ($arrcolsel[$tablecount]==$column) {
  //mysqli_query($dbnchopen,"SET NAMES 'utf8'");
  $timestamp='2015-01-01 00:00:00'; 
  $qryval = "SELECT * FROM ".$dbtable." WHERE flddbsyncstatus='SYNC' AND fldtimestamp>'".$timestamp."'";
  echo "<div class='alert alert-info'>";
  echo $qryval."<br>";
  echo "</div>";
  $resval = dbquerytyp($dbtyp,$dbnchopen,$qryval);
  echo "<br>";
  echo "<table class='table table-hover'>";
  echo "<tr><th>Index</th><th>Bezeichnung</th></tr>";
  $datcnt=0;
  while ($linval = dbfetchtyp($dbtyp,$resval)) {
    $datcnt=$datcnt+1;
    echo "<tr>";
    echo "<td>".$linval['fldIndex']."</td>";
    if ($utf8encode="J") {
      echo "<td>".utf8_encode($linval['fldBez'])."</td>";
    } else {
      echo "<td>".$linval['fldBez']."</td>";
    }
    echo "</tr>";
  }
  echo "</table>";
  echo "<input type='hidden' name='stranzds' value=".$datcnt." />";
  echo "<input type='submit' value='Daten einspielen' />";
  echo "</form>";
} else {
  echo "<div class='alert alert-info'>";
  echo "nach:".$column."<br>";
  echo "von :".$dbcolumn."<br>";
  echo $dbtable." no ok.";
  echo "</div>";
}

bootstrapend();
?>