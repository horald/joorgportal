<?php

function showauswahl($gdbcon,$menu,$idwert,$auto) {
  $value="Sync starten";
  echo "<br>";
  echo "<form class='form-horizontal' method='post' action='sync.php?showsync=1&menu=".$menu."&idwert=".$idwert."'>";
  
  $fquery = "SELECT * FROM tbldatabase ";
  $fresult = mysqli_query($gdbcon,$fquery);
  echo "<div class='control-group'>";
  echo "  <label class='control-label' style='text-align:left' for='input01'>Datenbank:</label>";
  echo "  <select name='datenbank' size='1'>";
  while ($fline = mysqli_fetch_array($fresult,MYSQLI_ASSOC)) {
    echo "<option style='background-color:#c0c0c0;' value='".$fline['fldindex']."' >".$fline['fldbemerk']."</option>";
  }
  echo "  </select>";
  echo "</div>";
  
  
  echo "<div class='control-group'>";
  echo "  <label class='control-label' style='text-align:left' for='input01'>Direktion:</label>";
  echo "<select name='typ' size='1'>";
  echo "<option style='background-color:#c0c0c0;' selected>local</option>";
  echo "<option style='background-color:#c0c0c0;' >remote</option>";
  echo "</select>";
  echo "</div>";
  echo "          <div class='control-group'>";
  echo "            <div class='checkbox'>";
  echo "              <input type='checkbox' name='debug'> Zeig Fehlermeldung";
  echo "            </div>";
  echo "            <div class='checkbox'>";
  echo "              <input type='checkbox' name='utf8encode'> utf8 encode";
  echo "            </div>";
  echo "          </div>";
  echo "<br>";
  echo "<input type='hidden' name='status' value='sync' />";
  if ($auto=="J") {
    echo "<input type='hidden' name='auto' value=".$auto." />";
    echo "<input type='submit' name='submit' id='btnsubmit' value='Submit' />";
  } else {
    echo "<dd><input type='submit' value='".$value."' /></dd>";
  }
  echo "</form>";
}

function auslesen() {
  echo "auslesen<br>";
}

function fernabfrage($gdbcon,$menu,$idwert,$dbase,$dbtable,$debug,$utf8encode) {
  $aktpfad=$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];  
  $callbackurl="http://".$aktpfad."?menu=".$menu."&idwert=".$idwert;

  $dbnchopen=dbopentyp("MYSQL","dbjoorgportal","root","mysql");
  $dbcolumn=getdbcolumn("MYSQL","dbjoorgportal",$dbtable,"root","mysql");
  //echo $dbcolumn."=dbcolumn<br>";
  
  $fquery = "SELECT * FROM tbldatabase WHERE fldindex=".$dbase;
  $fresult = mysqli_query($gdbcon,$fquery);
  $fline = mysqli_fetch_array($fresult,MYSQLI_ASSOC);
  $website=$fline['fldpfad']."classes/syncauslesen.php?menu=".$menu."&idwert=".$idwert."&debug=".$debug;
  echo "<div class='alert alert-info'>";
  if ($debug="J") {
  	 echo "website=".$website;
  	 echo "<br>";
  }
  echo "Remote-Abfrage<br>"; 
  echo $fline['fldbemerk']." auslesen.";
  echo "</div>";
  echo "<form class='form-horizontal' method='post' action='".$website."'>";
  echo "<input type='hidden' name='callbackurl' value='".$callbackurl."' />";
  echo "<input type='hidden' name='utf8encode' value='".$utf8encode."' />";
  echo "<input type='hidden' name='dbtable' value='".$dbtable."' />";
  echo "<input type='hidden' name='dbtyp' value='".$fline['flddbtyp']."' />";
  echo "<input type='hidden' name='dbcolumn' value='".json_encode($dbcolumn)."' />";
  echo "<input type='hidden' name='database' value='".$fline['flddbbez']."' />";
  echo "<input type='hidden' name='dbuser' value='".$fline['flddbuser']."' />";
  echo "<input type='hidden' name='dbpassword' value='".$fline['flddbpassword']."' />";
  echo "<input type='submit' value='Daten einspielen' /><br>";
  echo "</form>";
}

?>