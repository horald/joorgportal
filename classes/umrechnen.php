<?php
include("../config.php");
include("bootstrapfunc.php");
include("umrechnenfunc.php");
$menu = $_GET['menu'];
include("../sites/views/wp_".$menu."/showtab.inc.php");
bootstraphead();
bootstrapbegin("Umrechnen");
$idwert = $_GET['idwert'];
$umrechnen = $_GET['umrechnen'];
if ($umrechnen==1) {
  $errmsg=$_POST['errmsg'];
  if (isset($_REQUEST['submit'])) { 
    $portion = $_POST['portion'];
    $bez = $_POST['bez'];
    speicher_umrechnung($gdbcon,$pararray,$menu,$idwert,$portion,$bez);
  } else {
    echo "Vorgang abgebrochen";
    if ($errmsg<>true) {
      echo "<meta http-equiv='refresh' content='0; URL=showtab.php?menu=".$menu."&idwert=".$idwert."'>";    
    }
  }
} else {
  abfrage_umrechnen($gdbcon,$pararray,$menu,$idwert);
}
bootstrapend();
?>