<?php
include ("../config.php");
include("bootstrapfunc.php");
bootstraphead();
bootstrapbegin("Extrapunkte");
$benutzerid=$_GET['benutzerid'];
$idwert=$_GET['idwert'];
$qryuser="SELECT * FROM tblbenutzer WHERE fldindex=".$benutzerid;
$resuser = mysqli_query($gdbcon,$qryuser);
$linuser = mysqli_fetch_array($resuser,MYSQLI_ASSOC);
$benutzer=$linuser['fldbez'];
$qrytodo="SELECT * FROM tbltodo WHERE fldindex=".$idwert;
mysqli_query($gdbcon,"SET NAMES 'utf8'");
$restodo = mysqli_query($gdbcon,$qrytodo);
$lintodo = mysqli_fetch_array($restodo,MYSQLI_ASSOC);
$aufgabenid = $lintodo['fldid_belohnaufgabe'];
$aufgabe = $lintodo['fldbez'];
$bisdatum = $lintodo['fldbisdatum'];
$heute=date("Y-m-d");
$boolextrapunkte=true;
if ($lintodo['flderledigt']=="J") {
  $boolextrapunkte=false;
  $erldatum=substr($lintodo['flderldatum'],8,2).".".substr($lintodo['flderldatum'],5,2).".".substr($lintodo['flderldatum'],0,4);
  $text="Extrapunkte wurden bereits am ".$erldatum." vergeben!</br>";
}
if ($heute>$bisdatum) {
  $boolextrapunkte=false;
  $text="Leider ist die Frist (".$bisdatum.") abgelaufen.<br>";
}
if ($boolextrapunkte==false) {
  echo "<div class='alert alert-danger'>";
  echo $text;
  echo "Es können keine Extrapunkte für ".$benutzer." vergeben werden.";
  echo "</div>";
} else {
  $qrypunkte="SELECT * FROM tblbelohnpunkte WHERE fldid_aufgabe=".$aufgabenid." AND fldid_user=".$benutzerid;
  $respunkte = mysqli_query($gdbcon,$qrypunkte);
  $linpunkte = mysqli_fetch_array($respunkte,MYSQLI_ASSOC);
  $punkte=5;
  if ($linpunkte['fldpunkte']==0) {
    $extrapunkte=5;
  } else {
    $extrapunkte=$linpunkte['fldpunkte']*5;
  }
  $zeigheute=date("d.m.Y");
  $qrytermin="SELECT * FROM tblbelohntermin WHERE fldid_aufgabe=".$aufgabenid."  AND fldid_user=".$benutzerid."  AND flddatum='".$heute."'";
  $restermin = mysqli_query($gdbcon,$qrytermin);
  if ($lintermin = mysqli_fetch_array($restermin,MYSQLI_ASSOC)) {
    $punkte=$punkte+$lintermin['fldpunkte'];	
    $qrybelohn="UPDATE tblbelohntermin SET fldpunkte=".$punkte." WHERE fldid_aufgabe=".$aufgabenid."  AND fldid_user=".$benutzerid."  AND flddatum='".$heute."'";
    $text=$benutzer." erhält ".$extrapunkte." Extrapunkte für '".$aufgabe."' am ".$zeigheute." (Punkte werden aktualisiert.)";
  } else {
    $qrybelohn="INSERT INTO tblbelohntermin (fldid_aufgabe,fldid_user,flddatum,fldpunkte) VALUES(".$aufgabenid.",".$benutzerid.",'".$heute."',".$punkte.")";	
    $text=$benutzer." erhält ".$extrapunkte." Extrapunkte für '".$aufgabe."' am ".$zeigheute." (Punkte werden neu eingetragen.)";
  }
  mysqli_query($gdbcon,$qrybelohn);
  $qryerl="UPDATE tbltodo SET flderledigt='J', flderldatum='".$heute."' WHERE fldindex=".$idwert;
  mysqli_query($gdbcon,$qryerl);
  echo "<div class='alert alert-info'>";
  echo $text;
  echo "</div>";
}
echo "<a class='btn btn-primary' href='showtab.php?menu=todo&benutzerid=".$benutzerid."'>zurueck</a>";
bootstrapend();
?>