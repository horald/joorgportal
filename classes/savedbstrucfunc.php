<?php
header("content-type: text/html; charset=utf-8");

function savedbstruc_abfrage() {

  echo "<form name='eingabe' class='form-horizontal' method='post' action='savedbstruc.php?savedbstruc=1'>";
  echo "  <div class='control-group'>";
  echo "    <label class='control-label' style='text-align:left' for='input01'>Datenstruktur sichern?</label>";
  echo "  </div>";
  echo "  <div class='form-actions'>";
  echo "     <button type='submit' name='submit' class='btn btn-primary'>Speichern</button>";
  echo "     <button class='btn'>Abbruch</button>";
  echo "  </div>";
  echo "</form>";
}

function savedbstruc() {
  include("../config.php");
  $string = file_get_contents("../version.json");
  $json_a = json_decode($string, true);
  $versnr=$json_a['versnr'];
  echo "<div class='alert alert-success'>";
  echo "Datenbankstruktur sichern fuer Version ".$versnr."<br>";
  echo "</div>";
  $query="SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='dbjoorgportal'";
  //echo $query."<br>";
  $result = mysqli_query($gdbcon,$query);
  echo "<div class='alert alert-information'>";
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    echo $line['TABLE_NAME']."<br>";
  }
  echo "======================================================================<br>";
  $query="SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='tblversion' AND TABLE_SCHEMA='dbjoorgportal'";
  //echo $query."<br>";
  $result = mysqli_query($gdbcon,$query);
  while ($line = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    echo $line['COLUMN_NAME']."<br>";
  }
  echo "</div>";
}

?>

