<?php
header("content-type: text/html; charset=utf-8");

?>
<script>
function phponchange(id,menu,idwert,dbfield) {
//alert('phponchange:'+menu);
//window.location.href = "callphp.php?id=" + escape(id) + "&menu=" + escape(menu) + "&idwert=" + escape(idwert) + "&dbfield=" + escape(dbfield);    
}	
</script>
<?php

function schnellerfass_abfrage($gdbcon,$listarray,$menu,$idwert,$keyort,$ortvalue,$savetyp,$showbild) {
  echo "<a href='showtab.php?menu=".$menu."&idwert=".$idwert."' class='btn btn-primary btn-sm active' role='button'>Zurück</a>"; 
  echo "<form class='form-horizontal' method='post' action='schnellerfass.php?schnellerfass=1&menu=".$menu."&idwert=".$idwert."'>";
  foreach ( $listarray as $arrelement ) {
    if (!(($arrelement['fieldsave']=="NO") || ($arrelement['schnellerfass']=="NO"))) {
      $default="";
      $defwert='';
      if ($arrelement['default']!="") {
        $default=$arrelement['default'];
      }
      if ($arrelement['name']<>"") {
        if ($arrelement['getdefault']=="true") {
          $defquery="SELECT * FROM tblfilter WHERE fldmaske='".strtoupper($menu)."_DEFAULT' AND fldName='".$arrelement['name']."'";
          $defresult = mysqli_query($gdbcon,$defquery);
          if ($defrow = mysqli_fetch_array($defresult,MYSQLI_ASSOC)) {
        	   $defwert=$defrow['fldwert'];
          }	
        }  
      }  
      if ($arrelement['schnellerfass']=="key") {
        echo "<input type='hidden' name='key' value='".$arrelement['dbfield']."'/>";
	  }
      if ($arrelement['schnellerfass']=="ort") {
        echo "<input type='hidden' name='ort' value='".$arrelement['dbfield']."'/>";
	  }
      if ($arrelement['schnellerfass']=="mhdatum") {
        echo "<input type='hidden' name='ort' value='".$arrelement['dbfield']."'/>";
      }
      if ($arrelement['schnellerfass']=="anz") {
        $defwert=1;
      }
      switch ( $arrelement['type'] )
      {
        case 'bild':
          $default="";
          if ($showbild=="Y") {
            $pfad="/var/www/html/daten/bilder";
            $d = dir($pfad);
            $lastdate="";
            $actfile="";
            while (false !== ($entry = $d->read())) {
          	  if($entry != "." && $entry != ".."){
          	    $actdate=date ("Y-m-d H:i:s", filectime($pfad."/".$entry));	
                //echo $entry." ".$actdate."<br>";
                if ($actdate>$lastdate) {
              	   $lastdate=$actdate; 
              	   $actfile=$entry;
              	   $default="http://192.168.0.156/daten/bilder/".$entry;
                }
              }
            }
            $d->close();
          }
  echo "<table>";
  echo "<tr>";
  echo "<td>";
          echo "<dl>";
          echo "<dt><label >".$arrelement['label'].":</label> ".$actfile."</dt>";
          echo "<dd>";
          echo "<input type='text' name='".$arrelement['dbfield']."' value='".$default."' /> ";
          echo "<a href='schnellerfass.php?schnellerfass=2&keyort=".$keyort."&ort=".$ortvalue."&menu=".$menu."' class='btn btn-info' role='button'>Aktualisieren</a> ";
          echo "<a href='schnellerfass.php?schnellerfass=3&keyort=".$keyort."&ort=".$ortvalue."&menu=".$menu."' class='btn btn-info' role='button'>ohne Bild</a> ";
          echo "</dd>";
          echo "</dl>";
  echo "</td>";
  if ($showbild=="Y") {
    echo "<td> <img src='".$default."' alt='Artikel Bild' height='128' width='128'> </td>";
  } else {
    echo "<td>  </td>";
  }  
  echo "</tr>";
  echo "</table>";
		    echo "<input type='hidden' name='showbild' value='".$showbild."' />";
        break;	
        case 'text':
		    if ($defwert<>"") {
		      $default=$defwert;
		    }
          if ($arrelement['barcode']=="YES") {
            if ($_GET['barcode']<>"") {
		    	  $default=substr($_GET['barcode'],0,13);
		      }
		    }  
		    $getbez="";	
          if (isset($_GET['bez'])) { 
            if ($_GET['dbfield']==$arrelement['dbfield']) {
              $getbez=$_GET['bez'];
              $default=$_GET['id'];
            }
          }
          if ($keyort==$arrelement['dbfield']) {
            $default=$ortvalue;
          }
          echo "<dl>";
          echo "<dt><label >".$arrelement['label'].":</label></dt>";
          echo "<dd><input type='text' name='".$arrelement['dbfield']."' value='".$default."' onchange='phponchange(this.value,".chr(34).$menu.chr(34).",".chr(34).$idwert.chr(34).",".chr(34).$arrelement['dbfield'].chr(34).");' /> ";
          if ($arrelement['barcode']=="YES") {
            echo "<a href='getbarcode.php?menu=".$menu."' class='btn btn-info' role='button'>Barcode</a>";
          }  
          if ($arrelement['ortkurz']=="YES") {
            echo "<a href='getortkurz.php?menu=".$menu."&default=".$default."' class='btn btn-info' role='button'>Ort</a>";
            $sqlsel="SELECT * FROM tblortkurz WHERE fldkurz='".$default."'";
            $ressel = mysqli_query($gdbcon,$sqlsel);
            if ($rowsel = mysqli_fetch_array($ressel,MYSQLI_ASSOC)) {
              $bez=$rowsel['fldbez'];
            } else {
              $bez="";            
            }           
            echo " ".$bez;
          } 
          echo " ".$getbez;
          echo "</dd>";
          echo "</dl>";
          break;
        case 'date':
          echo "<tr>";
          echo "<td class='col-md-1'><label >".$arrelement['label'].":</label></td>";
          echo "<td class='col-md-2'><div class='input-group date form_date col-md-2' data-date='' data-date-format='yyyy-mm-dd' data-link-field='dtp_input2' data-link-format='yyyy-mm-dd'>";
          echo "<input class='form-control' size='8' type='text' name='".$arrelement['dbfield']."' value='".$arr[$arrelement['dbfield']]."' >";
		    echo "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>";
          echo "</div>";
		    echo "<input type='hidden' id='dtp_input2' value='' /><br/></td>";
          echo "</tr>";
          break;
        case 'select':
          $seldbwhere="";
          if ($arrelement['seldbwhere']<>"") {
            $seldbwhere=" WHERE ".$arrelement['seldbwhere'];
          }
          $sqlsel="SELECT * FROM ".$arrelement['dbtable'].$seldbwhere;
          $ressel = mysqli_query($gdbcon,$sqlsel);
          echo "<dl>";
          echo "<dt><label >".$arrelement['label'].":</label></dt>";
          echo "<select name='".$arrelement['dbfield']."' size='1'>";
          echo "<option style='background-color:#c0c0c0;' >(ohne)</option>";
          while ($rowsel = mysqli_fetch_array($ressel,MYSQLI_ASSOC)) {
            if ($defwert==$rowsel[$arrelement['seldbfield']]) {
              echo "<option style='background-color:#c0c0c0;' selected>".$rowsel[$arrelement['seldbfield']]."</option>";
            } else {
              echo "<option style='background-color:#c0c0c0;' >".$rowsel[$arrelement['seldbfield']]."</option>";
            }
          }
          echo "</select> ";
          echo "</dl>";
		  break;
      }
    }
  }
  echo "<select name='savetyp' size='1'>";
  if ($savetyp=="Hinzufügen") {
    echo "<option style='background-color:#c0c0c0;' selected>Hinzufügen</option>";
  } else {
    echo "<option style='background-color:#c0c0c0;' >Hinzufügen</option>";
  }
  if ($savetyp=="Aktualisieren") {
    echo "<option style='background-color:#c0c0c0;' selected>Aktualisieren</option>";
  } else {  
    echo "<option style='background-color:#c0c0c0;' >Aktualisieren</option>";
  }  
  if ($savetyp=="vermindern") {
    echo "<option style='background-color:#c0c0c0;' selected>vermindern</option>";
  } else {
    echo "<option style='background-color:#c0c0c0;' >vermindern</option>";
  }
  echo "</select><br>";
  echo "<input type='checkbox' name='chkanzeigen' value='anzeigen'> Speichern anzeigen<br>";
  echo "<input type='submit' name='submit' value='OK' /></dd>";
  echo "</form>";  
}

function schnellerfass_verarbeiten($gdbcon,$pararray,$listarray,$submit,$key,$keyvalue,$keyort,$ortvalue,$show,$autoinc_start,$autoinc_step,$menu,$savetyp) {
  $dbtable=$pararray['dbtable'];
  $qryort = "SELECT * FROM tblortkurz WHERE fldkurz='".$ortvalue."'";
  $resort = mysqli_query($gdbcon,$qryort);
  $idzimmer=0;
  if ($rowort = mysqli_fetch_array($resort,MYSQLI_ASSOC)) {
    $_POST['fldid_zimmer']=$rowort['fldid_zimmer'];
    $idzimmer=$rowort['fldid_zimmer'];
  }
  $select="SELECT * FROM ".$dbtable." WHERE ".$key."='".$keyvalue."' AND ".$keyort."='".$ortvalue."'";
  $results = mysqli_query($gdbcon,$select);
  if ($row = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
    $anz=0;
    $sql="UPDATE ".$dbtable." SET fldid_zimmer=".$idzimmer.", ";
    foreach ( $listarray as $arrelement ) {
      if ($arrelement['fieldsave']<>"NO") {
        switch ( $arrelement['schnellerfass'] ) {
          case 'anz':
            switch ( $savetyp ) {
              case 'Hinzufügen':	
			       $chgwert=$_POST[$arrelement['dbfield']]; 
                $anz=$row[$arrelement['dbfield']]+$_POST[$arrelement['dbfield']];
                break;
              case 'Aktualisieren':	
                $anz=$_POST[$arrelement['dbfield']];
                break;
              case 'vermindern':	
			       $chgwert=$_POST[$arrelement['dbfield']]; 
                $anz=$row[$arrelement['dbfield']]-$_POST[$arrelement['dbfield']];
                break;
            }
            $sql=$sql.$arrelement['dbfield']."='".$anz."', ";
          break;
        }
      }
    } 
    $sql=substr($sql,0,-2);
    $sql=$sql." WHERE fldindex=".$row['fldindex'];
  } else {
  	 $anz=1;
    $sqlid="SELECT * FROM tblindex WHERE fldtable='".$pararray['dbtable']."'";
    //echo $sqlid."<br>";
    $resid = mysqli_query($gdbcon,$sqlid);
    if ($rowid = mysqli_fetch_array($resid,MYSQLI_ASSOC)) {
      $newrowid=$rowid['fldid'] + $autoinc_step;
      //echo $newrowid."=newrowid<br>";
    } else {
      $newrowid=$autoinc_start;  
    }
    //echo "INSERT<br>";
    //$sql="INSERT INTO ".$dbtable." (".$pararray['fldindex'].",";
    $sql="INSERT INTO ".$dbtable." (";
    foreach ( $listarray as $arrelement ) {
  	  if ($arrelement['fieldsave']<>"NO") {
	    $sql=$sql.$arrelement['dbfield'].",";
      }
    } 
    $sql=substr($sql,0,-1);
    if ($pararray['dbsyncnr']=="J") {
   	  $sql=$sql.",flddbsyncnr";
  	  $sql=$sql.",fldtimestamp";
    }  
    //$sql=$sql.") VALUES (".$newrowid.",";
    $sql=$sql.") VALUES (";
    foreach ( $listarray as $arrelement ) {
  	  if ($arrelement['fieldsave']<>"NO") {
  	    if ($arrelement['schnellerfass']=="anz") {
		  $chgwert=$_POST[$arrelement['dbfield']];
		 }
        if ($arrelement['type']=="selectid") {
          if ($_POST[$arrelement['dbfield']]=="") {
            $sql=$sql."0,";
          } else {	
            $sql=$sql.$_POST[$arrelement['dbfield']].",";
          }
        } else { 
          $sql=$sql."'".$_POST[$arrelement['dbfield']]."',";
        }
	  }
	}  
    $sql=substr($sql,0,-1);
    if ($pararray['dbsyncnr']=="J") {
  	  $sql=$sql.",".$autoinc_start;
  	  $sql=$sql.",datetime('now', 'localtime')";
    }
    $sql=$sql.")";
	
    $sqlupd="UPDATE tblindex SET fldid=".$newrowid."  WHERE fldtable='".$pararray['dbtable']."'";
    mysqli_query($gdbcon,$sqlup);
	
  }
  //echo $sql."<br>";
  mysqli_query($gdbcon,$sql);

  $artikel=""; 
  $qryart="SELECT * FROM tblartikel WHERE ".$key."='".$keyvalue."'"; 
  //echo $qryart."<br>";
  $resart = mysqli_query($gdbcon,$qryart);
  if ($rowart = mysqli_fetch_array($resart,MYSQLI_ASSOC)) {
    $artikel=" (".$rowart['fldBez'].")";
  }
  echo "<div class='alert alert-success'>";
  switch ( $savetyp ) {
    case 'Hinzufügen':	
	  echo "Artikelbestand von ".$keyvalue.$artikel." mit ".$chgwert." auf ".$anz." erhöht.<br>";
      break;
    case 'Aktualisieren':	
	  echo "Artikelbestand von ".$keyvalue.$artikel." auf ".$anz." aktualisiert.<br>";
      break;
    case 'vermindern':	
	  echo "Artikelbestand von ".$keyvalue.$artikel." mit ".$chgwert." auf ".$anz." vermindert.<br>";
      break;
  }
  echo "</div>";  
  
    foreach ( $listarray as $arrelement ) {
  	  if ($arrelement['fieldsave']<>"NO") {
        if ($arrelement['getdefault']=="true") {
            $sqlflt="SELECT * FROM tblfilter WHERE fldmaske='".strtoupper($menu)."_DEFAULT' AND fldName='".$arrelement['name']."'";
            //echo $sqlflt."<br>";
            $resflt = mysqli_query($gdbcon,$qryflt);
            if ($rowflt = mysqli_fetch_array($resflt,MYSQLI_ASSOC)) {
              $sqldef="UPDATE tblfilter SET fldwert='".$_POST[$arrelement['dbfield']]."' WHERE fldmaske='".strtoupper($menu)."_DEFAULT' AND fldName='".$arrelement['name']."'"; 
            } else {
              $sqldef="INSERT INTO tblfilter (fldfeld,fldmaske,fldName,fldwert) VALUES('".$arrelement['dbfield']."','".strtoupper($menu)."_DEFAULT','".$arrelement['name']."','".$_POST[$arrelement['dbfield']]."')";
            }
            //echo $sqldef."<br>";
            $qrydef = mysqli_query($gdbcon,$sqldef);
        }
      }
	}
	
  if ($show=="anzeigen") {
    echo "<div class='alert alert-success'>";
    echo $sql."<br>";
    //echo $db->lastErrorMsg()."<br>";
    echo "</div>";
  }  
	  
}?>