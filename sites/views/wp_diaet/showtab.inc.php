<?php
$listarray = array ( array ( 'label' => 'Gewicht',
                             'name' => 'gewicht', 
                             'width' => 50, 
                             'type' => 'average',
                             'dbfield' => 'fldgewicht' ),
                     array ( 'label' => 'Uhrzeit',
                             'name' => 'uhrzeit', 
                             'width' => 150, 
                             'type' => 'text',
                             'dbfield' => 'flduhrzeit' ),
                     array ( 'label' => 'Datum',
                             'name' => 'datum', 
                             'width' => 90, 
                             'type' => 'date',
                             'dbfield' => 'flddatum' ),
                     array ( 'label' => 'Benutzer',
                             'name' => 'benutzer', 
                             'width' => 80, 
                             'type' => 'selectid',
                             'dbtable' => 'tblbenutzer',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_benutzer' ));

$filterarray = array ( array ( 'label' => 'Benutzer',
                             'name' => 'fltdiaetbenutzer', 
                             'width' => 10, 
                             'type' => 'selectid',
                             'sign' => '=',
                             'dbtable' => 'tblbenutzer',
                             'seldbfield' => 'fldbez',
                             'seldbindex' => 'fldindex',
                             'dbfield' => 'fldid_benutzer' ));
							 

$pararray = array ( 'headline' => 'Diät',
                    'dbtable' => 'tbldiaet',
                    'orderby' => 'flddatum,flduhrzeit',
                    'fldindex' => 'fldindex');
?>