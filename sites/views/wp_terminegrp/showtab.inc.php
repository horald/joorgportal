<?php
$listarray = array ( array ( 'label' => 'Bezeichnung',
                             'name' => 'bez', 
                             'width' => 200, 
                             'type' => 'text',
                             'dbfield' => 'fldbez' ),
                     array ( 'label' => 'Kurz',
                             'name' => 'kurz', 
                             'width' => 200, 
                             'type' => 'text',
                             'dbfield' => 'fldkurz' ),
                     array ( 'label' => 'Farbe',
                             'name' => 'farbe', 
                             'width' => 200, 
                             'type' => 'text',
                             'dbfield' => 'fldfarbe' ),
                     array ( 'label' => 'Jährlich',
                             'name' => 'jaehrlich', 
                             'width' => 200, 
                             'type' => 'YN',
                             'dbfield' => 'fldjaehrlich' ));


$pararray = array ( 'headline' => 'Terminegruppen',
                    'dbtable' => 'tbltermine_grp',
                    'orderby' => '',
                    'strwhere' => '',
                    'fldbez' => 'fldbez',
                    'fldindex' => 'fldindex');
?>