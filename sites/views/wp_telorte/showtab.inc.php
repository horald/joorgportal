<?php
$listarray = array ( array ( 'label' => 'Sort',
                             'name' => 'sort', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldsort' ),
                     array ( 'label' => 'Bezeichnung',
                             'name' => 'bez', 
                             'width' => 100, 
                             'type' => 'text',
                             'dbfield' => 'fldbez' ),
                     array ( 'label' => 'Telefon-IP',
                             'name' => 'telefonip', 
                             'width' => 50, 
                             'type' => 'text',
                             'dbfield' => 'fldtelefonip' ),
                     array ( 'label' => 'Farbe',
                             'name' => 'farbe', 
                             'width' => 50, 
                             'type' => 'text',
                             'dbfield' => 'fldfarbe' ));


$pararray = array ( 'headline' => 'Orte',
                    'dbtable' => 'tbltelorte',
                    'orderby' => 'fldsort',
                    'strwhere' => '',
                    'fldindex' => 'fldindex');
?>